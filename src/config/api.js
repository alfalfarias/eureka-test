export const api = {
  url: process.env.VUE_APP_API_URL,
  key: process.env.VUE_APP_API_KEY,
};

export default api;