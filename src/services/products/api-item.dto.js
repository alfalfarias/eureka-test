const dto = function (item) {
  const element = {
    title: item.title && String(item.title),
    product_type: item.product_type && String(item.product_type),
    image: item.image && String(item.image),
    quantitySold: item.quantitySold && Number(item.quantitySold),
    variants: [] && item.variants,
  };
  return element;
};

export default dto;