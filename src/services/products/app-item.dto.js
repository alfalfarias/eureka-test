const dto = function (item) {
  const element = {
    title: item.title && String(item.title),
    productType: item.productType && String(item.productType),
    image: item.image && String(item.image),
    quantitySold: item.quantitySold && Number(item.quantitySold),
    variantTitle: item.variantTitle && String(item.variantTitle),
    variantPrice: item.variantPrice && Number(item.variantPrice),
  };
  return element;
};

export default dto;