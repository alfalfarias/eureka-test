import { api } from '@/config/api';

export const service = {
  async fetchAll() {
    const http = await fetch(`${api.url}/api/v1/products/best-selling-products-by-subcategory`, {
      headers: {
        'Content-Type': 'application/json',
        'secretKey': `${api.key}`,
      }
    });
    const response = await http.json();

    let items = [];
    const products = response;
    for (let i = 0; i < products.length; i++) {
      const product = products[i];
      const variants = product.variants;
      for (let j = 0; j < variants.length; j++) {
        const variant = variants[j];
        const item = {
          id: product.id,
          title: product.title,
          productType: product.product_type,
          image: product.image.src,
          quantitySold: product.quantitySold,
          isOffer: product.tags.includes('#offer'),
          variantId: variant.id,
          variantTitle: variant.title,
          variantPrice: variant.price,
        };
        items.push(item);
      }
    }
    const data = items;
    return data;
  },
};

export default service;